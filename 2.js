function calcular() {
    // Valores introducidos por el usuario
    let nombre = document.querySelector('#nombre').value;
    let apellidos = document.querySelector('#apellidos').value;

    // Concatenamos el nombre y los apellidos
    let nombreCompleto = nombre + " " + apellidos;
    // Modificamos el contenido del div donde mostramos el resultado
    document.querySelector('#salida').innerHTML = nombreCompleto;
}