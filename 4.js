//constante que apunta al boton que tiene que hacer el evento
const boton = document.querySelector('button');

// Usamos una función anónima y metemos el código dentro de la función anónima

boton.addEventListener("click", function () {
    let nombre = document.querySelector('#nombre').value;
    let apellidos = document.querySelector('#apellidos').value;

    let nombreCompleto = nombre + " " + apellidos;

    document.querySelector('#salida').innerHTML = nombreCompleto;
});

