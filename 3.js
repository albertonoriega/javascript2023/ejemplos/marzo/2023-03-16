// Codigo que introduces para que se ejecute el JS cuando se haya cargado la página
// Solo es necesario si en el HTML pones el script encima del body
//document.addEventListener("load",carga);

//constante que apunta al boton que tiene que hacer el evento
const boton = document.querySelector('button');

// Añadimos manejadores de eventos a las etiquetas de HTML

boton.addEventListener("click", calcular);

function calcular() {
    // Valores introducidos por el usuario
    let nombre = document.querySelector('#nombre').value;
    let apellidos = document.querySelector('#apellidos').value;

    // Concatenamos el nombre y los apellidos
    let nombreCompleto = nombre + " " + apellidos;
    // Modificamos el contenido del div donde mostramos el resultado
    document.querySelector('#salida').innerHTML = nombreCompleto;
}